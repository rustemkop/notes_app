<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    // Отношение многие ко многим с постами
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
