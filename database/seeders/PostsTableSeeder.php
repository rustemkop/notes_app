<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post1 = Post::create([
            'title' => 'First Post',
            'content' => 'Content of the first post.',
            'is_published' => true,
        ]);

        $post2 = Post::create([
            'title' => 'Second Post',
            'content' => 'Content of the second post.',
            'is_published' => true,
        ]);

        // Привязываем категории к постам (пример связи многие ко многим)
        $category1 = Category::find(1);
        $category2 = Category::find(2);

        $post1->categories()->attach([$category1->id, $category2->id]);
        $post2->categories()->attach([$category2->id]);
    }
}
