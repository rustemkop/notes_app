<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddUsersAndRolesSeeder extends Seeder
{
    private $permissions = [
        'role-list',
        'role-create',
        'role-edit',
        'role-delete',
        'post-list',
        'post-create',
        'post-edit',
        'post-delete'
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@test.kz',
            'password' => Hash::make('password')
        ]);

        $user2 = User::create([
            'name' => 'moderator',
            'email' => 'moderator@test.kz',
            'password' => Hash::make('modpassword')
        ]);

        $roleAdmin = Role::create(['name' => 'admin', 'guard_name' =>'web']);
        $roleModerator = Role::create(['name' => 'moderator', 'guard_name' =>'web']);


        $permissions = Permission::pluck('id', 'id')->all();

        $permissionForAdmin = collect($permissions)->reject(function ($value, $key) {
            return $key == 2;
        })->all();
        $permissionsForModerator = Permission::where('id', 6)->first();


        $roleAdmin->syncPermissions($permissionForAdmin);

        $user->assignRole([$roleAdmin->id]);

        $roleModerator->syncPermissions($permissionsForModerator);

        $user2->assignRole([$roleAdmin->id]);
    }
}
