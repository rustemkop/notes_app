<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    if (auth()->check()) return redirect('/dashboard'); // the route you want the user to be redirected to.
    return view('login');
});

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('auth');
Route::post('/login', [AuthController::class, 'login']);
Route::get('/logout', [AuthController::class, 'logout']);
Route::post('/edit/{id}', [PostController::class, 'edit'])->name('posts.edit');
Route::delete('/{id}', [PostController::class, 'destroy'])->name('posts.destroy');
