@extends('layouts.app')


@section('content')
    <div class="container mx-auto pt-12">
        <h1>Welcome, <span class="font-bold">{{ $user->name }}</span></h1>
    </div>

    @foreach ($posts as $post)
    <div class="sm:grid grid-cols-2 gap-20 w-4/5 mx-auto py-15 border-b border-gray-200">
        <div>
            <img src="{{ asset('images/' . $post->image_path) }}" alt="">
        </div>
        <div>
            <h2 class="text-gray-3700 font-bold text-3xl pb-4">
                {{ $post->title }}
            </h2>

            <p class="text-2xl text-gray-700 pt-8 pb-10 leading-8 font-light">
                {{substr($post->content, 0, 250)}} <span class="text-3xl">...</span>
            </p>

            <div class="mt-5 mb-10 sm:mt-8 sm:flex sm:justify-center lg:justify-start">
                <div class="rounded-md shadow">
                    <a href="/post/{{ $post->id }}" class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10">
                        Read More
                    </a>
                </div>

            </div>

            @if (isset($user))
            <span class="float-right">
                <a href="/post/{{ $post->id }}/edit" class="text-gray-700 italic hover:text-gray-900 hover:bg-green-500 hover:px-4 hover:text-gray-100 pb-1 border-b-2">
                    Edit
                </a>
            </span>

            <span class="float-right">
                <form action="{{ route('posts.destroy', $post->id) }}" method="POST" id="delete-form">
                    @csrf
                    @method('delete')

                    <button class="text-red-500 pr-3" type="submit" onclick="if (confirm('Are you sure to delete this data?')) {
                        event.preventDefault();
                        document.getElementById('delete-form').submit();

                        }else{
                            event.preventDefault();
                        }
                    ">
                        Delete
                    </button>

                </form>
            </span>
            @endif
        </div>
    </div>
    @endforeach
@endsection
